extern crate ring;
use self::ring::{aead, pbkdf2, digest};

// Had to use AES256 cause 128 wasn't working. Possibly key and/or nonce too big.
pub fn aes256_enc(data: &[u8], pass: &[u8], salt: &[u8]) -> Vec<u8> {
    let mut inout = data.to_vec();
    let mut key = [0; digest::SHA256_OUTPUT_LEN];
    pbkdf2::derive(&digest::SHA256, 100, &salt, &pass[..], &mut key);

    for _ in 0..aead::AES_256_GCM.tag_len() {
        inout.push(0);
    }

    let seal = aead::SealingKey::new(&aead::AES_256_GCM, &key).unwrap();
    let _size = aead::seal_in_place(&seal, &[0;12], &[], &mut inout,
                                    aead::AES_256_GCM.tag_len()).unwrap();
    inout
}

pub fn aes256_dec(data: &[u8], pass: &[u8], salt: &[u8]) -> Option<Vec<u8>> {
    let mut inout = data.to_vec();
    let mut key = [0; digest::SHA256_OUTPUT_LEN];
    pbkdf2::derive(&digest::SHA256, 100, &salt, &pass[..], &mut key);

    let seal = aead::OpeningKey::new(&aead::AES_256_GCM, &key).unwrap();
    match aead::open_in_place(&seal, &[0;12], &[], 0, &mut inout) {
        Err(_) => None,
        Ok(dec) => Some(dec.to_vec())
    }
}