#![windows_subsystem = "windows"]
#![feature(plugin)]

#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
use rocket::Config;
use rocket::config::Environment;
use rocket::response::NamedFile;
use rocket::http::RawStr;
use rocket::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::http::Status;

mod crypt;

mod hotkey_settings;
use hotkey_settings::HotkeySettings;

extern crate systray;

extern crate web_view;
use web_view::*;


#[macro_use] extern crate serde_json;
use serde_json::{Value, Error};

extern crate crosswin;
use crosswin::api as xwin;

extern crate inputbot;
use inputbot::KeybdKey;

use std::env;
use std::thread;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path,PathBuf};

extern crate base64;

struct WindowStatus {
    pub open: bool,
    pub ready: bool
}

static HOST: &str = "localhost:59433";

struct Addr(String);

// Check to make sure we're connecting through localhost. We don't want outsiders!
impl<'a, 'r> FromRequest<'a, 'r> for Addr {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Addr, ()> {
        let hosts: Vec<_> = request.headers().get("host").collect();
        if hosts.len() != 1 {
            return Outcome::Failure((Status::BadRequest, ()));
        }

        let host = hosts[0].to_string();
        if host == HOST {
            return Outcome::Success(Addr(host));
        }

        return Outcome::Failure((Status::BadRequest, ()));
    }
}

///
/// Get a path relative to the current working directory.
///
fn dir(path: &str) -> String {
    return format!("{}\\{}", env::current_dir().unwrap().display(), path);
}

///
/// Gets a path relative to the website's working directory.
///
fn cdfile(path: &str) -> String
{
    return format!("file:///{}/view/{}", env::current_dir().unwrap().display(), path);
}

fn error_dlg(view: &mut WebView<Value>, e: &str) {
    view.dialog( Dialog::Alert(Alert::Error), "Error!", e );
}

#[allow(unused_must_use)]
fn invoke(view: &mut WebView<Value>, arg: &str) {
    //let userdata = view.user_data_mut();
    match arg {
        "test" => {
            println!("AH!");
        },
        "RK~appload" => {
            unsafe{MAIN_STATUS.ready = true;}
            // TODO: stuff
            view.eval(r#"
                        waitHideLoad(255);
                    "#);
            println!("MAIN: {}", view.user_data());
        },
        "RK~miniload" => {
            unsafe{MINI_STATUS.ready = true;}
            // TODO: stuff
            view.eval(r#"
                        waitHideLoad(255);
                    "#);
            println!("MINI: {}", view.user_data());
        },
        "RK~close" => {
            view.terminate();
        },
        _ => {
            if arg.starts_with("RK~gid:") { // They entered a private key for some reason.
                let (_, gid) = arg.split_at(7);
                view.user_data_mut()["gid"] = Value::String(gid.to_string());
                println!("{}", view.user_data());
            } else {
                println!("{}", arg)
            }
        }
    }
}

#[get("/")]
fn index(addr: Addr) -> rocket::response::content::Html<String>  {
    let mut f = File::open("./view/index.html").expect("File not found");
    let mut cont = String::new();
    f.read_to_string(&mut cont).expect("Failed to read main content");
    cont = cont.replace("{{GROUPS}}", &format!("{}", ""));
    rocket::response::content::Html(cont)
}


#[get("/mini")]
fn mini(addr: Addr) -> rocket::response::content::Html<String>  {
    let mut f = File::open("./view/mini.html").expect("File not found");
    let mut cont = String::new();
    f.read_to_string(&mut cont).expect("Failed to read mini content");
    cont = cont.replace("{{GROUPS}}", &format!("{}", ""));
    rocket::response::content::Html(cont)
}

#[get("/list?<search>")]
fn mini_srch(search: &RawStr, addr: Addr) -> rocket::response::content::Html<String>  {
    let mut f = File::open("./view/pages/pitem.html").expect("File not found");
    let mut cont: String = String::new();
    f.read_to_string(&mut cont).expect("Failed to read list page content");
    cont = cont.replace("{{LIST}}", &format!("<b>{}</b>", search.as_str()));
    rocket::response::content::Html(cont)
}


#[get("/css/<file..>")]
fn static_css(file: PathBuf, addr: Addr) -> rocket::response::content::Css<String>/*Option<NamedFile>*/ {
    //NamedFile::open(Path::new("./view/css/").join(file)).ok()
    let mut f = File::open(Path::new("./view/css/").join(file)).expect("File not found");
    let mut cont = String::new();
    f.read_to_string(&mut cont).expect("Failed to read css content");
    rocket::response::content::Css(cont)
}

#[get("/img/<file..>")]
fn static_img(file: PathBuf, addr: Addr) -> Option<NamedFile> {
    NamedFile::open(Path::new("./view/img/").join(file)).ok()
}

#[get("/pages/<file..>")]
fn static_pages(file: PathBuf, addr: Addr) -> rocket::response::content::Plain<String> {
    let mut fr = File::open(Path::new("./view/pages/").join(file));
    let mut cont = String::new();
    if fr.is_ok() {
        fr.unwrap().read_to_string(&mut cont).expect("Failed to read page content");
        rocket::response::content::Plain(cont)
    } else {
        File::open(Path::new("./view/pages/404.html")).unwrap().read_to_string(&mut cont).expect("Failed to read page content");
        rocket::response::content::Plain(cont)
    }
}

#[get("/js/<file..>")]
fn static_js(file: PathBuf, addr: Addr) -> rocket::response::content::JavaScript<String>/*Option<NamedFile>*/ {
    //NamedFile::open(Path::new("./view/js/").join(file)).ok()
    let mut f = File::open(Path::new("./view/js/").join(file)).expect("File not found");
    let mut cont = String::new();
    f.read_to_string(&mut cont).expect("Failed to read js content");
    rocket::response::content::JavaScript(cont)
}

///
/// Spawns a thread for the main web view.
///
fn open_webview() {
    unsafe {
        thread::spawn(move ||{
            let userdata: Value = json!({
                            "state": 0
                        });
            MAIN_STATUS.open = true;
            WebViewBuilder::new()
                .title("RustyKeys")
                .resizable(false)
                .content(Content::Url(&format!("http://{}/rustykeys/", HOST)))
                .size(1280, 720)
                .debug(false)
                .user_data(userdata)
                .invoke_handler(|webview, arg| {
                    invoke(webview, arg); // I don't like handling stuff down here
                    Ok(())
                })
                .build().unwrap().run().unwrap();
            MAIN_STATUS.ready = false;
            MAIN_STATUS.open = false;
        });
    }
}

///
/// Function called when the global hotkey is pressed.
///
fn hotkey_callback() {
    unsafe {
        if !MINI_STATUS.open {
            thread::spawn(move ||{
                let userdata: Value = json!({
                            "state": 0
                        });
                MINI_STATUS.open = true;
                WebViewBuilder::new()
                    .title("RustyKeys")
                    .resizable(false)
                    .content(Content::Url(&format!("http://{}/rustykeys/mini", HOST)))
                    .size(1280/2, 720/2)
                    .debug(false)
                    .user_data(userdata)
                    .invoke_handler(|webview, arg| {
                        invoke(webview, arg); // I don't like handling stuff down here
                        Ok(())
                    })
                    .build().unwrap().run().unwrap();
                MINI_STATUS.ready = false;
                MINI_STATUS.open = false;
            });
            let ms5 = std::time::Duration::new(0,5);
            while !MINI_STATUS.ready {
                thread::sleep(ms5);
            }
        }
        let found = xwin::search_windows(1, true,
                                          "RustyKeys", "RustyKeys", false);
        found.first().unwrap().show();
    }
}


static mut MINI_STATUS: WindowStatus = WindowStatus{ open: false, ready: false };
static mut MAIN_STATUS: WindowStatus = WindowStatus{ open: false, ready: false };
//static mut IS_MINI_READY: bool = false;
//static mut IS_MAIN_OPEN: bool = false;
//static mut IS_MAIN_READY: bool = false;

static mut HOTKEY_SETTINGS: HotkeySettings = HotkeySettings {
    hotkey: KeybdKey::RKey,
    ctrl: true,
    alt: true,
    shift: true
};

fn main() {
    thread::spawn(move ||{ // Global hotkey thread
        unsafe { HOTKEY_SETTINGS.bind(hotkey_callback); }

        inputbot::handle_input_events();
    });

    thread::spawn(move ||{ // Web server thread
        let mut sec_key: Vec<u8> = String::from("RustyKeys").into_bytes();
        sec_key.resize(32, 8);

        let config = Config::build(Environment::Production)
            .port(59433)
            .secret_key(base64::encode(&sec_key)) // TODO: config file
            .log_level(rocket::config::LoggingLevel::Critical)
            .unwrap();
        rocket::custom(config).mount("/rustykeys",routes![
            index,
            mini_srch,
            mini,
            static_pages,
            static_css,
            static_img,
            static_js
        ]).launch();
    });

    // System tray icon stuff
    let mut app;
    match systray::Application::new() {
        Ok(w) => app = w,
        Err(_) => panic!("Can't create window!")
    }

    match app.set_icon_from_file(&dir("rustykeys.png")) {
        Ok(_) => {
            println!("Icon set!");
        },
        Err(e) => {
            println!("Failed to set icon, attempting ico");
            app.set_icon_from_file(&dir("rustykeys.ico")).ok();
        }
    }
    app.add_menu_item(&"Open".to_string(), |_| {
        unsafe {
            if !MAIN_STATUS.open {
                open_webview();
            }
        }
    }).ok();
    app.add_menu_item(&"Open Mini".to_string(), |_| {
        hotkey_callback();
    }).ok();
    /*app.add_menu_item(&"Print a thing".to_string(), |_| {
        println!("Printing a thing!");
    }).ok();
    app.add_menu_item(&"Add Menu Item".to_string(), |window| {
        window.add_menu_item(&"Interior item".to_string(), |_| {
            println!("what");
        }).ok();
        window.add_menu_separator().ok();
    }).ok();*/
    app.add_menu_separator().ok();
    app.add_menu_item(&"Quit".to_string(), |window| {
        window.quit();
    }).ok();
    println!("Waiting on message!");
    app.set_tooltip(&"RustyKeys".to_string());
    app.wait_for_message();
}
