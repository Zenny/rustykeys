use inputbot::KeybdKey;
use std::marker::{Sync, Send};

pub struct HotkeySettings {
    pub hotkey: KeybdKey,
    pub ctrl: bool,
    pub alt: bool,
    pub shift: bool
}

///
/// Change the bindings of global hotkeys.
///
impl HotkeySettings {
    pub fn bind<F>(&'static self, callback: F) where F: Fn() + Sync + Send + 'static {
        unsafe {
            #[cfg(windows)]
                self.hotkey.bind(move || {
                    let alt = KeybdKey::OtherKey(0x12).is_pressed(); // Windows alt code, both
                    let ctrl = KeybdKey::LControlKey.is_pressed() || KeybdKey::RControlKey.is_pressed();
                    let shift = KeybdKey::LShiftKey.is_pressed() || KeybdKey::RShiftKey.is_pressed();

                    if ctrl == self.ctrl && alt == self.alt
                        && shift == self.shift {
                        callback();
                    }
                });
            #[cfg(linux)]
                self.hotkey.bind(move || {
                    let alt = KeybdKey::OtherKey(0xFFE9).is_pressed() ||
                        KeybdKey::OtherKey(0xFFEA).is_pressed(); // Linux alt code, left or right
                    let ctrl = KeybdKey::LControlKey.is_pressed() || KeybdKey::RControlKey.is_pressed();
                    let shift = KeybdKey::LShiftKey.is_pressed() || KeybdKey::RShiftKey.is_pressed();

                    if ctrl == self.ctrl && alt == self.alt
                        && shift == self.shift {
                        callback();
                    }
                });
        }
    }

    pub fn unbind(&self) {
        self.hotkey.unbind();
    }

    pub fn rebind<F>(&'static mut self, new_key: KeybdKey, ctrl: bool, alt: bool, shift: bool, callback: F)
        where F: Fn() + Sync + Send + 'static {
        self.unbind();
        self.hotkey = new_key;
        self.ctrl = ctrl;
        self.alt = alt;
        self.shift = shift;
        self.bind(callback);
    }
}