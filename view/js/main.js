'use strict';

let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
let debug = true;
let page = $("#mainpage");
let curPage = "";
let isMouseDown = false;
let isReady = true;

function invoke(s) {
    window.external.invoke(s);
}

function change()
{
    document.getElementById("btn1").innerText = "Test "+ window.location ;
}

function load(file)
{
    window.location.replace(file);
}

function hideLoad() // temp
{
    setTimeout(function(){ // Artificial for now, lets the transition set up.
        hideID('load-wrap')
    }, 150);
}

function waitHideLoad(i) // temp
{
    setTimeout(function(){ // Artificial for now, lets the transition set up.
        hideID('load-wrap')
    }, i);
}

function hideID(id)
{
    let el = $("#"+id);
    el.addClass("invis");
    setTimeout(function(){
        el.addClass("height0");
    }, 255); // After the invis transition
}
function fadeoutID(id, callback)
{
    let el = $("#"+id);
    el.fadeOut(143, function(){
        el.addClass("height0");
        if(typeof(callback) !== "undefined")
            callback();
    });
}
function fadeinID(id, callback)
{
    let el = $("#"+id);
    el.removeClass("height0");
    el.fadeIn(143, function(){
        if(typeof(callback) !== "undefined")
            callback();
    });
}

function hideDelID(id)
{
    let el = $("#"+id);
    el.addClass("invis");
    setTimeout(function(){
        el.remove();
    }, 255); // After the invis transition
}

function waitHideDelID(id, wait)
{
    setTimeout(function(){
        let el = $("#"+id);
        el.addClass("invis");
        setTimeout(function(){
            el.remove();
        }, 255); // After the invis transition
    }, wait); // After the invis transition
}

function showID(id)
{
    let el = $("#"+id);
    el.removeClass("height0");
    el.removeClass("invis");
}

function del(id)
{
    $("#"+id).remove();
}

function disableTabs(id)
{ //foreach child set data-tab to tabindex and tabindex to -1
    $("#"+id).children().attr("tabindex", "-1");
}

function enableTabs(id)
{
    $("#"+id).children().removeAttr("tabindex");
}

function click_signin() {
    let auth = gapi.auth2.getAuthInstance();
    auth.signIn({ux_mode:"redirect", redirect_uri:"http://localhost:59433/rustykeys/"});
}

// Sets the view to a page in the pages folder
function go(p) {
    if(isReady && curPage !== p) {
        isReady = false;
        fadeinID("pageload-wrap", function() {
            $.get("/rustykeys/pages/"+p+".html", function(d) {
                curPage = p;
                page.html(d);
                fadeoutID("pageload-wrap", function(){
                    isReady = true;
                });
            });
        });
    }

}

// Sets the view to a page in the pages folder without showing the loading screen.
function goNoLoad(p) {
    if(isReady && curPage !== p) {
        isReady = false;
        $.get("/rustykeys/pages/" + p + ".html", function (d) {
            curPage = p;
            page.html(d);
            isReady = true;
        });
    }
}

// Sets the view to a page at the root
function goRaw(s) {
    if(isReady && curPage !== s) {
        isReady = false;
        fadeinID("pageload-wrap", function () {
            $.get("/rustykeys/"+s, function (d) {
                curPage = s;
                page.html(d);
                fadeoutID("pageload-wrap", function(){
                    isReady = true;
                });
            });
        });
    }
}

// Sets the view to a page at the root without showing the loading screen.
function goRawNoLoad(s) {
    if(isReady && curPage !== s) {
        isReady = false;
        $.get("/rustykeys/"+s, function (d) {
            curPage = s;
            page.html(d);
            isReady = true;
        });
    }
}

$(function(){
    if(isChrome && debug)
    {
        hideLoad();
    }
    $(".noselect").attr('unselectable','on').on('selectstart', false).on('mousedown', false);
    let icons = $(".grpicon");
    icons.on("click.select", function(){
        if(isReady) {
            let el = $(this);
            icons.removeClass("selgrp");
            el.addClass("selgrp");
            if(el.is("[name]"))
                goRaw("group?name="+el.attr("name"));

        }
    });
});

